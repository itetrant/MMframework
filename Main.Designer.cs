﻿
using System.Windows.Forms;

namespace MMframework
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.lblWelcome = new System.Windows.Forms.Label();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.pnlTopRight = new System.Windows.Forms.Panel();
            this.pnlWelcome = new System.Windows.Forms.Panel();
            this.btnLogout = new FontAwesome.Sharp.IconButton();
            this.pnlMenuEnv = new System.Windows.Forms.Panel();
            this.btnMenu = new FontAwesome.Sharp.IconButton();
            this.pnlLogo = new System.Windows.Forms.Panel();
            this.lblApp = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.pnlContent = new System.Windows.Forms.Panel();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.pnlMenu3_sub = new System.Windows.Forms.Panel();
            this.btnMenu3_sub3 = new System.Windows.Forms.Button();
            this.lblLine6 = new System.Windows.Forms.Label();
            this.btnMenu3_sub2 = new System.Windows.Forms.Button();
            this.btnMenu3_sub1 = new System.Windows.Forms.Button();
            this.pnlMenu3 = new System.Windows.Forms.Panel();
            this.lblLine7 = new System.Windows.Forms.Label();
            this.btnMenu3 = new System.Windows.Forms.Button();
            this.pnlMenu2_sub = new System.Windows.Forms.Panel();
            this.lblLine5 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.pnlMenu2 = new System.Windows.Forms.Panel();
            this.btnMenu2 = new System.Windows.Forms.Button();
            this.lblLine4 = new System.Windows.Forms.Label();
            this.pnlMenu1_sub = new System.Windows.Forms.Panel();
            this.lblLine3 = new System.Windows.Forms.Label();
            this.btnMenu1_sub3 = new System.Windows.Forms.Button();
            this.btnMenu1_sub2 = new System.Windows.Forms.Button();
            this.btnMenu1_sub1 = new System.Windows.Forms.Button();
            this.pnlMenu1 = new System.Windows.Forms.Panel();
            this.btnMenu1 = new FontAwesome.Sharp.IconButton();
            this.lblLine2 = new System.Windows.Forms.Label();
            this.lblLine1 = new System.Windows.Forms.Label();
            this.pnlHome = new System.Windows.Forms.Panel();
            this.pnlHomeCenter = new System.Windows.Forms.Panel();
            this.btnHome = new FontAwesome.Sharp.IconButton();
            this.btnMenuSelected = new System.Windows.Forms.Button();
            this.pnlStatus = new System.Windows.Forms.Panel();
            this.lbStatusbar = new System.Windows.Forms.Label();
            this.pnlMid = new System.Windows.Forms.Panel();
            this.pnlTop.SuspendLayout();
            this.pnlTopRight.SuspendLayout();
            this.pnlWelcome.SuspendLayout();
            this.pnlMenuEnv.SuspendLayout();
            this.pnlLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.pnlMenu.SuspendLayout();
            this.pnlMenu3_sub.SuspendLayout();
            this.pnlMenu3.SuspendLayout();
            this.pnlMenu2_sub.SuspendLayout();
            this.pnlMenu2.SuspendLayout();
            this.pnlMenu1_sub.SuspendLayout();
            this.pnlMenu1.SuspendLayout();
            this.pnlHome.SuspendLayout();
            this.pnlHomeCenter.SuspendLayout();
            this.pnlStatus.SuspendLayout();
            this.pnlMid.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblWelcome
            // 
            this.lblWelcome.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblWelcome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblWelcome.Location = new System.Drawing.Point(323, 0);
            this.lblWelcome.Margin = new System.Windows.Forms.Padding(3, 0, 5, 0);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.lblWelcome.Size = new System.Drawing.Size(197, 48);
            this.lblWelcome.TabIndex = 0;
            this.lblWelcome.Text = "Welcome: User Name";
            this.lblWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblWelcome.UseCompatibleTextRendering = true;
            this.lblWelcome.UseMnemonic = false;
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.SystemColors.Window;
            this.pnlTop.Controls.Add(this.pnlTopRight);
            this.pnlTop.Controls.Add(this.pnlLogo);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(784, 48);
            this.pnlTop.TabIndex = 8;
            this.pnlTop.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pnlTopRight
            // 
            this.pnlTopRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(108)))), ((int)(((byte)(168)))));
            this.pnlTopRight.Controls.Add(this.lblWelcome);
            this.pnlTopRight.Controls.Add(this.pnlWelcome);
            this.pnlTopRight.Controls.Add(this.pnlMenuEnv);
            this.pnlTopRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTopRight.Location = new System.Drawing.Point(182, 0);
            this.pnlTopRight.Name = "pnlTopRight";
            this.pnlTopRight.Size = new System.Drawing.Size(602, 48);
            this.pnlTopRight.TabIndex = 7;
            // 
            // pnlWelcome
            // 
            this.pnlWelcome.Controls.Add(this.btnLogout);
            this.pnlWelcome.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlWelcome.Location = new System.Drawing.Point(520, 0);
            this.pnlWelcome.Name = "pnlWelcome";
            this.pnlWelcome.Size = new System.Drawing.Size(82, 48);
            this.pnlWelcome.TabIndex = 5;
            // 
            // btnLogout
            // 
            this.btnLogout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnLogout.ForeColor = System.Drawing.Color.White;
            this.btnLogout.IconChar = FontAwesome.Sharp.IconChar.PowerOff;
            this.btnLogout.IconColor = System.Drawing.Color.White;
            this.btnLogout.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnLogout.IconSize = 18;
            this.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.Location = new System.Drawing.Point(0, 0);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(82, 48);
            this.btnLogout.TabIndex = 0;
            this.btnLogout.Text = "Logout";
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogout.UseVisualStyleBackColor = true;
            // 
            // pnlMenuEnv
            // 
            this.pnlMenuEnv.Controls.Add(this.btnMenu);
            this.pnlMenuEnv.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenuEnv.ForeColor = System.Drawing.Color.White;
            this.pnlMenuEnv.Location = new System.Drawing.Point(0, 0);
            this.pnlMenuEnv.Name = "pnlMenuEnv";
            this.pnlMenuEnv.Size = new System.Drawing.Size(108, 48);
            this.pnlMenuEnv.TabIndex = 4;
            // 
            // btnMenu
            // 
            this.btnMenu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnMenu.FlatAppearance.BorderSize = 0;
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu.ForeColor = System.Drawing.Color.Transparent;
            this.btnMenu.IconChar = FontAwesome.Sharp.IconChar.Bars;
            this.btnMenu.IconColor = System.Drawing.Color.White;
            this.btnMenu.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnMenu.IconSize = 32;
            this.btnMenu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu.Location = new System.Drawing.Point(0, 0);
            this.btnMenu.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(108, 48);
            this.btnMenu.TabIndex = 0;
            this.btnMenu.Text = "UAT";
            this.btnMenu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMenu.UseVisualStyleBackColor = false;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // pnlLogo
            // 
            this.pnlLogo.BackColor = System.Drawing.Color.White;
            this.pnlLogo.Controls.Add(this.lblApp);
            this.pnlLogo.Controls.Add(this.picLogo);
            this.pnlLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLogo.Location = new System.Drawing.Point(0, 0);
            this.pnlLogo.Name = "pnlLogo";
            this.pnlLogo.Size = new System.Drawing.Size(182, 48);
            this.pnlLogo.TabIndex = 0;
            // 
            // lblApp
            // 
            this.lblApp.AutoSize = true;
            this.lblApp.BackColor = System.Drawing.SystemColors.Window;
            this.lblApp.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApp.Location = new System.Drawing.Point(80, 9);
            this.lblApp.Margin = new System.Windows.Forms.Padding(0);
            this.lblApp.Name = "lblApp";
            this.lblApp.Size = new System.Drawing.Size(95, 32);
            this.lblApp.TabIndex = 1;
            this.lblApp.Text = "MMVN";
            this.lblApp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblApp.Click += new System.EventHandler(this.label1_Click);
            // 
            // picLogo
            // 
            this.picLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            this.picLogo.InitialImage = ((System.Drawing.Image)(resources.GetObject("picLogo.InitialImage")));
            this.picLogo.Location = new System.Drawing.Point(0, 0);
            this.picLogo.Name = "picLogo";
            this.picLogo.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.picLogo.Size = new System.Drawing.Size(79, 48);
            this.picLogo.TabIndex = 6;
            this.picLogo.TabStop = false;
            // 
            // pnlContent
            // 
            this.pnlContent.BackColor = System.Drawing.SystemColors.Window;
            this.pnlContent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContent.Location = new System.Drawing.Point(182, 0);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.Size = new System.Drawing.Size(602, 452);
            this.pnlContent.TabIndex = 10;
            this.pnlContent.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.SystemColors.Control;
            this.pnlMenu.Controls.Add(this.pnlMenu3_sub);
            this.pnlMenu.Controls.Add(this.pnlMenu3);
            this.pnlMenu.Controls.Add(this.pnlMenu2_sub);
            this.pnlMenu.Controls.Add(this.pnlMenu2);
            this.pnlMenu.Controls.Add(this.pnlMenu1_sub);
            this.pnlMenu.Controls.Add(this.pnlMenu1);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(182, 452);
            this.pnlMenu.TabIndex = 9;
            // 
            // pnlMenu3_sub
            // 
            this.pnlMenu3_sub.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlMenu3_sub.Controls.Add(this.btnMenu3_sub3);
            this.pnlMenu3_sub.Controls.Add(this.lblLine6);
            this.pnlMenu3_sub.Controls.Add(this.btnMenu3_sub2);
            this.pnlMenu3_sub.Controls.Add(this.btnMenu3_sub1);
            this.pnlMenu3_sub.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu3_sub.Location = new System.Drawing.Point(0, 285);
            this.pnlMenu3_sub.Name = "pnlMenu3_sub";
            this.pnlMenu3_sub.Size = new System.Drawing.Size(182, 86);
            this.pnlMenu3_sub.TabIndex = 9;
            // 
            // btnMenu3_sub3
            // 
            this.btnMenu3_sub3.BackColor = System.Drawing.SystemColors.MenuBar;
            this.btnMenu3_sub3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenu3_sub3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMenu3_sub3.FlatAppearance.BorderSize = 0;
            this.btnMenu3_sub3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu3_sub3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnMenu3_sub3.Location = new System.Drawing.Point(0, 56);
            this.btnMenu3_sub3.Margin = new System.Windows.Forms.Padding(0);
            this.btnMenu3_sub3.Name = "btnMenu3_sub3";
            this.btnMenu3_sub3.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.btnMenu3_sub3.Size = new System.Drawing.Size(175, 28);
            this.btnMenu3_sub3.TabIndex = 13;
            this.btnMenu3_sub3.Text = ">> Menu3_sub3";
            this.btnMenu3_sub3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu3_sub3.UseVisualStyleBackColor = false;
            this.btnMenu3_sub3.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblLine6
            // 
            this.lblLine6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblLine6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLine6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblLine6.Location = new System.Drawing.Point(0, 84);
            this.lblLine6.Name = "lblLine6";
            this.lblLine6.Size = new System.Drawing.Size(182, 2);
            this.lblLine6.TabIndex = 13;
            // 
            // btnMenu3_sub2
            // 
            this.btnMenu3_sub2.BackColor = System.Drawing.SystemColors.MenuBar;
            this.btnMenu3_sub2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenu3_sub2.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMenu3_sub2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMenu3_sub2.FlatAppearance.BorderSize = 0;
            this.btnMenu3_sub2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu3_sub2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnMenu3_sub2.Location = new System.Drawing.Point(0, 28);
            this.btnMenu3_sub2.Margin = new System.Windows.Forms.Padding(0);
            this.btnMenu3_sub2.Name = "btnMenu3_sub2";
            this.btnMenu3_sub2.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.btnMenu3_sub2.Size = new System.Drawing.Size(182, 28);
            this.btnMenu3_sub2.TabIndex = 12;
            this.btnMenu3_sub2.Text = ">> Menu3_sub2";
            this.btnMenu3_sub2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu3_sub2.UseVisualStyleBackColor = false;
            this.btnMenu3_sub2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // btnMenu3_sub1
            // 
            this.btnMenu3_sub1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.btnMenu3_sub1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenu3_sub1.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMenu3_sub1.FlatAppearance.BorderSize = 0;
            this.btnMenu3_sub1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu3_sub1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnMenu3_sub1.Location = new System.Drawing.Point(0, 0);
            this.btnMenu3_sub1.Margin = new System.Windows.Forms.Padding(0);
            this.btnMenu3_sub1.Name = "btnMenu3_sub1";
            this.btnMenu3_sub1.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.btnMenu3_sub1.Size = new System.Drawing.Size(182, 28);
            this.btnMenu3_sub1.TabIndex = 11;
            this.btnMenu3_sub1.Text = ">> Menu3_sub1";
            this.btnMenu3_sub1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu3_sub1.UseVisualStyleBackColor = false;
            this.btnMenu3_sub1.Click += new System.EventHandler(this.button4_Click);
            // 
            // pnlMenu3
            // 
            this.pnlMenu3.Controls.Add(this.lblLine7);
            this.pnlMenu3.Controls.Add(this.btnMenu3);
            this.pnlMenu3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu3.Location = new System.Drawing.Point(0, 249);
            this.pnlMenu3.Name = "pnlMenu3";
            this.pnlMenu3.Size = new System.Drawing.Size(182, 36);
            this.pnlMenu3.TabIndex = 15;
            // 
            // lblLine7
            // 
            this.lblLine7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblLine7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLine7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblLine7.Location = new System.Drawing.Point(0, 34);
            this.lblLine7.Name = "lblLine7";
            this.lblLine7.Size = new System.Drawing.Size(182, 2);
            this.lblLine7.TabIndex = 14;
            // 
            // btnMenu3
            // 
            this.btnMenu3.BackColor = System.Drawing.SystemColors.Menu;
            this.btnMenu3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenu3.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMenu3.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnMenu3.FlatAppearance.BorderSize = 0;
            this.btnMenu3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu3.Image = ((System.Drawing.Image)(resources.GetObject("btnMenu3.Image")));
            this.btnMenu3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu3.Location = new System.Drawing.Point(0, 0);
            this.btnMenu3.Margin = new System.Windows.Forms.Padding(0);
            this.btnMenu3.Name = "btnMenu3";
            this.btnMenu3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnMenu3.Size = new System.Drawing.Size(182, 37);
            this.btnMenu3.TabIndex = 10;
            this.btnMenu3.Text = "Reports";
            this.btnMenu3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMenu3.UseVisualStyleBackColor = false;
            this.btnMenu3.Click += new System.EventHandler(this.btnMenu3_Click);
            // 
            // pnlMenu2_sub
            // 
            this.pnlMenu2_sub.Controls.Add(this.lblLine5);
            this.pnlMenu2_sub.Controls.Add(this.button3);
            this.pnlMenu2_sub.Controls.Add(this.button5);
            this.pnlMenu2_sub.Controls.Add(this.button6);
            this.pnlMenu2_sub.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu2_sub.Location = new System.Drawing.Point(0, 163);
            this.pnlMenu2_sub.Name = "pnlMenu2_sub";
            this.pnlMenu2_sub.Size = new System.Drawing.Size(182, 86);
            this.pnlMenu2_sub.TabIndex = 7;
            // 
            // lblLine5
            // 
            this.lblLine5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblLine5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLine5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblLine5.Location = new System.Drawing.Point(0, 84);
            this.lblLine5.Name = "lblLine5";
            this.lblLine5.Size = new System.Drawing.Size(182, 2);
            this.lblLine5.TabIndex = 12;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.MenuBar;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.button3.Location = new System.Drawing.Point(0, 56);
            this.button3.Margin = new System.Windows.Forms.Padding(0);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.button3.Size = new System.Drawing.Size(182, 28);
            this.button3.TabIndex = 9;
            this.button3.Text = ">> Menu2_sub3";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.MenuBar;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.button5.Location = new System.Drawing.Point(0, 28);
            this.button5.Margin = new System.Windows.Forms.Padding(0);
            this.button5.Name = "button5";
            this.button5.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.button5.Size = new System.Drawing.Size(182, 28);
            this.button5.TabIndex = 8;
            this.button5.Text = ">> Menu2_sub2";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.MenuBar;
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.Dock = System.Windows.Forms.DockStyle.Top;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.button6.Location = new System.Drawing.Point(0, 0);
            this.button6.Margin = new System.Windows.Forms.Padding(0);
            this.button6.Name = "button6";
            this.button6.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.button6.Size = new System.Drawing.Size(182, 28);
            this.button6.TabIndex = 7;
            this.button6.Text = ">> Menu2_sub1";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // pnlMenu2
            // 
            this.pnlMenu2.Controls.Add(this.btnMenu2);
            this.pnlMenu2.Controls.Add(this.lblLine4);
            this.pnlMenu2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu2.Location = new System.Drawing.Point(0, 125);
            this.pnlMenu2.Name = "pnlMenu2";
            this.pnlMenu2.Size = new System.Drawing.Size(182, 38);
            this.pnlMenu2.TabIndex = 15;
            // 
            // btnMenu2
            // 
            this.btnMenu2.BackColor = System.Drawing.SystemColors.Menu;
            this.btnMenu2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenu2.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMenu2.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnMenu2.FlatAppearance.BorderSize = 0;
            this.btnMenu2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu2.Image = ((System.Drawing.Image)(resources.GetObject("btnMenu2.Image")));
            this.btnMenu2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu2.Location = new System.Drawing.Point(0, 0);
            this.btnMenu2.Margin = new System.Windows.Forms.Padding(0);
            this.btnMenu2.Name = "btnMenu2";
            this.btnMenu2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnMenu2.Size = new System.Drawing.Size(182, 37);
            this.btnMenu2.TabIndex = 6;
            this.btnMenu2.Text = "Invoice Control";
            this.btnMenu2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMenu2.UseVisualStyleBackColor = false;
            this.btnMenu2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblLine4
            // 
            this.lblLine4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblLine4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLine4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblLine4.Location = new System.Drawing.Point(0, 36);
            this.lblLine4.Name = "lblLine4";
            this.lblLine4.Size = new System.Drawing.Size(182, 2);
            this.lblLine4.TabIndex = 11;
            // 
            // pnlMenu1_sub
            // 
            this.pnlMenu1_sub.Controls.Add(this.lblLine3);
            this.pnlMenu1_sub.Controls.Add(this.btnMenu1_sub3);
            this.pnlMenu1_sub.Controls.Add(this.btnMenu1_sub2);
            this.pnlMenu1_sub.Controls.Add(this.btnMenu1_sub1);
            this.pnlMenu1_sub.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu1_sub.Location = new System.Drawing.Point(0, 39);
            this.pnlMenu1_sub.Name = "pnlMenu1_sub";
            this.pnlMenu1_sub.Size = new System.Drawing.Size(182, 86);
            this.pnlMenu1_sub.TabIndex = 1;
            // 
            // lblLine3
            // 
            this.lblLine3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblLine3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLine3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblLine3.Location = new System.Drawing.Point(0, 84);
            this.lblLine3.Name = "lblLine3";
            this.lblLine3.Size = new System.Drawing.Size(182, 2);
            this.lblLine3.TabIndex = 12;
            // 
            // btnMenu1_sub3
            // 
            this.btnMenu1_sub3.BackColor = System.Drawing.SystemColors.MenuBar;
            this.btnMenu1_sub3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenu1_sub3.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMenu1_sub3.FlatAppearance.BorderSize = 0;
            this.btnMenu1_sub3.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnMenu1_sub3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu1_sub3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnMenu1_sub3.Location = new System.Drawing.Point(0, 56);
            this.btnMenu1_sub3.Margin = new System.Windows.Forms.Padding(0);
            this.btnMenu1_sub3.Name = "btnMenu1_sub3";
            this.btnMenu1_sub3.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.btnMenu1_sub3.Size = new System.Drawing.Size(182, 28);
            this.btnMenu1_sub3.TabIndex = 5;
            this.btnMenu1_sub3.Text = ">> Menu1_sub3";
            this.btnMenu1_sub3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu1_sub3.UseVisualStyleBackColor = false;
            this.btnMenu1_sub3.Click += new System.EventHandler(this.btnMenu1_sub3_Click);
            // 
            // btnMenu1_sub2
            // 
            this.btnMenu1_sub2.BackColor = System.Drawing.SystemColors.MenuBar;
            this.btnMenu1_sub2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenu1_sub2.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMenu1_sub2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMenu1_sub2.FlatAppearance.BorderSize = 0;
            this.btnMenu1_sub2.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnMenu1_sub2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu1_sub2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnMenu1_sub2.Location = new System.Drawing.Point(0, 28);
            this.btnMenu1_sub2.Margin = new System.Windows.Forms.Padding(0);
            this.btnMenu1_sub2.Name = "btnMenu1_sub2";
            this.btnMenu1_sub2.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.btnMenu1_sub2.Size = new System.Drawing.Size(182, 28);
            this.btnMenu1_sub2.TabIndex = 4;
            this.btnMenu1_sub2.Text = ">> Menu1_sub2";
            this.btnMenu1_sub2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu1_sub2.UseVisualStyleBackColor = false;
            this.btnMenu1_sub2.Click += new System.EventHandler(this.btnMenu1_sub2_Click);
            // 
            // btnMenu1_sub1
            // 
            this.btnMenu1_sub1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.btnMenu1_sub1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenu1_sub1.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMenu1_sub1.FlatAppearance.BorderSize = 0;
            this.btnMenu1_sub1.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnMenu1_sub1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu1_sub1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnMenu1_sub1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnMenu1_sub1.Location = new System.Drawing.Point(0, 0);
            this.btnMenu1_sub1.Margin = new System.Windows.Forms.Padding(0);
            this.btnMenu1_sub1.Name = "btnMenu1_sub1";
            this.btnMenu1_sub1.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.btnMenu1_sub1.Size = new System.Drawing.Size(182, 28);
            this.btnMenu1_sub1.TabIndex = 3;
            this.btnMenu1_sub1.Text = ">> Menu1_sub1";
            this.btnMenu1_sub1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu1_sub1.UseVisualStyleBackColor = false;
            this.btnMenu1_sub1.Click += new System.EventHandler(this.btnMenu1_sub1_Click);
            // 
            // pnlMenu1
            // 
            this.pnlMenu1.Controls.Add(this.btnMenu1);
            this.pnlMenu1.Controls.Add(this.lblLine2);
            this.pnlMenu1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu1.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu1.Name = "pnlMenu1";
            this.pnlMenu1.Size = new System.Drawing.Size(182, 39);
            this.pnlMenu1.TabIndex = 0;
            // 
            // btnMenu1
            // 
            this.btnMenu1.BackColor = System.Drawing.SystemColors.Menu;
            this.btnMenu1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenu1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnMenu1.FlatAppearance.BorderSize = 0;
            this.btnMenu1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnMenu1.IconChar = FontAwesome.Sharp.IconChar.Cogs;
            this.btnMenu1.IconColor = System.Drawing.Color.DimGray;
            this.btnMenu1.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnMenu1.IconSize = 22;
            this.btnMenu1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu1.Location = new System.Drawing.Point(0, 0);
            this.btnMenu1.Name = "btnMenu1";
            this.btnMenu1.Size = new System.Drawing.Size(182, 37);
            this.btnMenu1.TabIndex = 0;
            this.btnMenu1.Text = "System Configuration";
            this.btnMenu1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMenu1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMenu1.UseVisualStyleBackColor = false;
            this.btnMenu1.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // lblLine2
            // 
            this.lblLine2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblLine2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLine2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblLine2.Location = new System.Drawing.Point(0, 37);
            this.lblLine2.Name = "lblLine2";
            this.lblLine2.Size = new System.Drawing.Size(182, 2);
            this.lblLine2.TabIndex = 13;
            // 
            // lblLine1
            // 
            this.lblLine1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblLine1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLine1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblLine1.Location = new System.Drawing.Point(0, 36);
            this.lblLine1.Name = "lblLine1";
            this.lblLine1.Size = new System.Drawing.Size(784, 2);
            this.lblLine1.TabIndex = 0;
            // 
            // pnlHome
            // 
            this.pnlHome.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlHome.Controls.Add(this.pnlHomeCenter);
            this.pnlHome.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHome.Location = new System.Drawing.Point(0, 48);
            this.pnlHome.Name = "pnlHome";
            this.pnlHome.Size = new System.Drawing.Size(784, 38);
            this.pnlHome.TabIndex = 11;
            // 
            // pnlHomeCenter
            // 
            this.pnlHomeCenter.BackColor = System.Drawing.SystemColors.Menu;
            this.pnlHomeCenter.Controls.Add(this.btnHome);
            this.pnlHomeCenter.Controls.Add(this.btnMenuSelected);
            this.pnlHomeCenter.Controls.Add(this.lblLine1);
            this.pnlHomeCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHomeCenter.Location = new System.Drawing.Point(0, 0);
            this.pnlHomeCenter.Name = "pnlHomeCenter";
            this.pnlHomeCenter.Size = new System.Drawing.Size(784, 38);
            this.pnlHomeCenter.TabIndex = 1;
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.SystemColors.Menu;
            this.btnHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.btnHome.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.btnHome.IconColor = System.Drawing.Color.DimGray;
            this.btnHome.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnHome.IconSize = 22;
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.Location = new System.Drawing.Point(0, -2);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(182, 37);
            this.btnHome.TabIndex = 3;
            this.btnHome.Text = "Home";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // btnMenuSelected
            // 
            this.btnMenuSelected.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnMenuSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnMenuSelected.Enabled = false;
            this.btnMenuSelected.FlatAppearance.BorderSize = 0;
            this.btnMenuSelected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenuSelected.Image = ((System.Drawing.Image)(resources.GetObject("btnMenuSelected.Image")));
            this.btnMenuSelected.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMenuSelected.Location = new System.Drawing.Point(0, 0);
            this.btnMenuSelected.Margin = new System.Windows.Forms.Padding(0);
            this.btnMenuSelected.Name = "btnMenuSelected";
            this.btnMenuSelected.Size = new System.Drawing.Size(784, 36);
            this.btnMenuSelected.TabIndex = 1;
            this.btnMenuSelected.Text = "Home";
            this.btnMenuSelected.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMenuSelected.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMenuSelected.UseVisualStyleBackColor = false;
            // 
            // pnlStatus
            // 
            this.pnlStatus.BackColor = System.Drawing.Color.LightGray;
            this.pnlStatus.Controls.Add(this.lbStatusbar);
            this.pnlStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlStatus.Location = new System.Drawing.Point(0, 538);
            this.pnlStatus.Name = "pnlStatus";
            this.pnlStatus.Size = new System.Drawing.Size(784, 23);
            this.pnlStatus.TabIndex = 12;
            // 
            // lbStatusbar
            // 
            this.lbStatusbar.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbStatusbar.AutoSize = true;
            this.lbStatusbar.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lbStatusbar.Location = new System.Drawing.Point(717, 3);
            this.lbStatusbar.Name = "lbStatusbar";
            this.lbStatusbar.Size = new System.Drawing.Size(55, 13);
            this.lbStatusbar.TabIndex = 0;
            this.lbStatusbar.Text = "Status bar";
            // 
            // pnlMid
            // 
            this.pnlMid.Controls.Add(this.pnlContent);
            this.pnlMid.Controls.Add(this.pnlMenu);
            this.pnlMid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMid.Location = new System.Drawing.Point(0, 86);
            this.pnlMid.Name = "pnlMid";
            this.pnlMid.Size = new System.Drawing.Size(784, 452);
            this.pnlMid.TabIndex = 13;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pnlMid);
            this.Controls.Add(this.pnlStatus);
            this.Controls.Add(this.pnlHome);
            this.Controls.Add(this.pnlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "MM Solution";
            this.Load += new System.EventHandler(this.Main_Load);
            this.pnlTop.ResumeLayout(false);
            this.pnlTopRight.ResumeLayout(false);
            this.pnlWelcome.ResumeLayout(false);
            this.pnlMenuEnv.ResumeLayout(false);
            this.pnlLogo.ResumeLayout(false);
            this.pnlLogo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.pnlMenu.ResumeLayout(false);
            this.pnlMenu3_sub.ResumeLayout(false);
            this.pnlMenu3.ResumeLayout(false);
            this.pnlMenu2_sub.ResumeLayout(false);
            this.pnlMenu2.ResumeLayout(false);
            this.pnlMenu1_sub.ResumeLayout(false);
            this.pnlMenu1.ResumeLayout(false);
            this.pnlHome.ResumeLayout(false);
            this.pnlHomeCenter.ResumeLayout(false);
            this.pnlStatus.ResumeLayout(false);
            this.pnlStatus.PerformLayout();
            this.pnlMid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlContent;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Panel pnlMenuEnv;
        private System.Windows.Forms.Panel pnlMenu1_sub;
        private System.Windows.Forms.Button btnMenu1_sub3;
        private System.Windows.Forms.Button btnMenu1_sub2;
        private System.Windows.Forms.Button btnMenu1_sub1;
        private System.Windows.Forms.Button btnMenu2;
        private System.Windows.Forms.Panel pnlMenu2_sub;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private Label lblApp;
        private Panel pnlHome;
        private Panel pnlStatus;
        private Panel pnlMid;
        private PictureBox picLogo;
        private Panel pnlTopRight;
        private Panel pnlMenu1;
        private Panel pnlHomeCenter;
        private Label lbStatusbar;
        private Button btnMenu3;
        private Panel pnlMenu3_sub;
        private Button btnMenu3_sub3;
        private Button btnMenu3_sub2;
        private Button btnMenu3_sub1;
        private Button btnMenuSelected;
        private Label lblLine1;
        private Label lblLine4;
        private Label lblLine2;
        private Label lblLine3;
        private Label lblLine5;
        private Label lblLine7;
        private Label lblLine6;
        private Panel pnlMenu2;
        private Panel pnlMenu3;
        private FontAwesome.Sharp.IconButton btnMenu;
        private Panel pnlLogo;
        private Panel pnlWelcome;
        private FontAwesome.Sharp.IconButton btnLogout;
        private FontAwesome.Sharp.IconButton btnMenu1;
        private FontAwesome.Sharp.IconButton btnHome;
    }
}