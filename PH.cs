﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMframework
{
    public partial class PH : Form
    {
        public PH()
        {
            InitializeComponent();
        }

        private void PH_Load(object sender, EventArgs e)
        {
            trvPH.ExpandAll();
        }

        // List the checked TreeNodes.
        private void btnShowChecked_Click(object sender, EventArgs e)
        {
            // Get the checked nodes.
            List<TreeNode> checked_nodes = CheckedNodes(trvPH);

            // Display the checked nodes.
            string results = "";
            foreach (TreeNode node in checked_nodes)
                results += node.Text + "\n";
            MessageBox.Show(results);
            Close();
        }

        // Return a list of the checked TreeView nodes.
        private List<TreeNode> CheckedNodes(TreeView trv)
        {
            List<TreeNode> checked_nodes = new List<TreeNode>();
            FindCheckedNodes(checked_nodes, trvPH.Nodes);
            return checked_nodes;
        }

        // Return a list of the TreeNodes that are checked.
        private void FindCheckedNodes(List<TreeNode> checked_nodes, TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                // Add this node.
                if (node.Checked) checked_nodes.Add(node);

                // Check the node's descendants.
                FindCheckedNodes(checked_nodes, node.Nodes);
            }
        }
    }
}
