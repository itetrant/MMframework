﻿namespace MMframework
{
    partial class PH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("AIR COOLER");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("MASSAGE, SEAT");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("AIR TREATMENT", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("HOUSEHOLD APPLIANCES", new System.Windows.Forms.TreeNode[] {
            treeNode3});
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("HOME APPLIANCE", new System.Windows.Forms.TreeNode[] {
            treeNode4});
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("RIBBON");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("PRINTING ACCESSORIES", new System.Windows.Forms.TreeNode[] {
            treeNode6});
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("OFFICE AUTOMATION", new System.Windows.Forms.TreeNode[] {
            treeNode7});
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("IT AND COM.ACC", new System.Windows.Forms.TreeNode[] {
            treeNode8});
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("HARD LINE", new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode9});
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Tuna Sandwich");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Milk");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Chips");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Hot Dog");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("HOME LINE", new System.Windows.Forms.TreeNode[] {
            treeNode11,
            treeNode12,
            treeNode13,
            treeNode14});
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("Lasagna");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("Bread");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("Wine");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Ice Cream");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("Cake");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("Brownie");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("Dessert", new System.Windows.Forms.TreeNode[] {
            treeNode19,
            treeNode20,
            treeNode21});
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("Salad");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("Rice");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("SOFT LINE", new System.Windows.Forms.TreeNode[] {
            treeNode16,
            treeNode17,
            treeNode18,
            treeNode22,
            treeNode23,
            treeNode24});
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("Node2");
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("DRY FOOD", new System.Windows.Forms.TreeNode[] {
            treeNode26});
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("Node3");
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("FRESH FOOD", new System.Windows.Forms.TreeNode[] {
            treeNode28});
            this.btnShowChecked = new System.Windows.Forms.Button();
            this.trvPH = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // btnShowChecked
            // 
            this.btnShowChecked.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnShowChecked.Location = new System.Drawing.Point(97, 273);
            this.btnShowChecked.Name = "btnShowChecked";
            this.btnShowChecked.Size = new System.Drawing.Size(99, 23);
            this.btnShowChecked.TabIndex = 8;
            this.btnShowChecked.Text = "OK";
            this.btnShowChecked.UseVisualStyleBackColor = true;
            this.btnShowChecked.Click += new System.EventHandler(this.btnShowChecked_Click);
            // 
            // trvPH
            // 
            this.trvPH.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trvPH.CheckBoxes = true;
            this.trvPH.FullRowSelect = true;
            this.trvPH.Location = new System.Drawing.Point(-3, 3);
            this.trvPH.Name = "trvPH";
            treeNode1.Name = "";
            treeNode1.Text = "AIR COOLER";
            treeNode2.Name = "";
            treeNode2.Text = "MASSAGE, SEAT";
            treeNode3.Name = "";
            treeNode3.Text = "AIR TREATMENT";
            treeNode4.Name = "";
            treeNode4.Text = "HOUSEHOLD APPLIANCES";
            treeNode5.Name = "";
            treeNode5.Text = "HOME APPLIANCE";
            treeNode6.Name = "Node10";
            treeNode6.Text = "RIBBON";
            treeNode7.Name = "Node9";
            treeNode7.Text = "PRINTING ACCESSORIES";
            treeNode8.Name = "Node8";
            treeNode8.Text = "OFFICE AUTOMATION";
            treeNode9.Name = "";
            treeNode9.Text = "IT AND COM.ACC";
            treeNode10.Name = "";
            treeNode10.Text = "HARD LINE";
            treeNode11.Name = "";
            treeNode11.Text = "Tuna Sandwich";
            treeNode12.Name = "";
            treeNode12.Text = "Milk";
            treeNode13.Name = "";
            treeNode13.Text = "Chips";
            treeNode14.Name = "";
            treeNode14.Text = "Hot Dog";
            treeNode15.Name = "";
            treeNode15.Text = "HOME LINE";
            treeNode16.Name = "";
            treeNode16.Text = "Lasagna";
            treeNode17.Name = "";
            treeNode17.Text = "Bread";
            treeNode18.Name = "";
            treeNode18.Text = "Wine";
            treeNode19.Name = "";
            treeNode19.Text = "Ice Cream";
            treeNode20.Name = "";
            treeNode20.Text = "Cake";
            treeNode21.Name = "";
            treeNode21.Text = "Brownie";
            treeNode22.Name = "";
            treeNode22.Text = "Dessert";
            treeNode23.Name = "";
            treeNode23.Text = "Salad";
            treeNode24.Name = "";
            treeNode24.Text = "Rice";
            treeNode25.Name = "";
            treeNode25.Text = "SOFT LINE";
            treeNode26.Name = "Node2";
            treeNode26.Text = "Node2";
            treeNode27.Name = "";
            treeNode27.Text = "DRY FOOD";
            treeNode28.Name = "Node3";
            treeNode28.Text = "Node3";
            treeNode29.Name = "";
            treeNode29.Text = "FRESH FOOD";
            this.trvPH.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode10,
            treeNode15,
            treeNode25,
            treeNode27,
            treeNode29});
            this.trvPH.Size = new System.Drawing.Size(311, 255);
            this.trvPH.TabIndex = 7;
            // 
            // PH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 308);
            this.Controls.Add(this.btnShowChecked);
            this.Controls.Add(this.trvPH);
            this.Name = "PH";
            this.Text = "Hierarchy selection";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.PH_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnShowChecked;
        internal System.Windows.Forms.TreeView trvPH;
    }
}

