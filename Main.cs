﻿using MMFramework;
using System;
using System.Linq;
using System.Windows.Forms;
namespace MMframework
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            customizeDesign();
        }
        private void customizeDesign()
        {
            pnlMenu1_sub.Visible = false;
            pnlMenu2_sub.Visible = false;
            pnlMenu3_sub.Visible = false;
 
        }

        private void showsubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                subMenu.Visible = true;
            }
            else subMenu.Visible = false;

        }

        private void Main_Load(object sender, EventArgs e)
        {
            frmLogin frm = new frmLogin();
            frm.IsLoggedIn = false;
            frm.ShowDialog();

            if (!frm.IsLoggedIn)
            {
                this.Close();
                Application.Exit();
                return;
            }
            lblWelcome.Text = "Welcome:" + frm.iUserName + " ";
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            showsubMenu(pnlMenu2_sub);
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnMenu1_sub1_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Form1>().Count() == 0) { 
                Form1 objForm = new Form1();
                objForm.TopLevel = false;
                pnlContent.Controls.Add(objForm);
                //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //objForm.Dock = DockStyle.Fill;
                objForm.Show();
                btnMenuSelected.Text = btnMenu1.Text + btnMenu1_sub1.Text;
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblAppName_Click(object sender, EventArgs e)
        {
            showsubMenu(pnlMenu);
            //showsubMenu(pnlHomeLeft);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void picLogo_Click(object sender, EventArgs e)
        {

        }

        private void btnMenu1_sub2_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Form1>().Count() == 0)
            {
                Form1 objForm = new Form1();
                objForm.TopLevel = false;
                pnlContent.Controls.Add(objForm);
                //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //objForm.Dock = DockStyle.Fill;
                objForm.Show();
                btnMenuSelected.Text = btnMenu1.Text + btnMenu1_sub2.Text;
            }
        }

        private void btnMenu1_sub3_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Form1>().Count() == 0)
            {
                Form1 objForm = new Form1();
                objForm.TopLevel = false;
                pnlContent.Controls.Add(objForm);
                //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //objForm.Dock = DockStyle.Fill;
                objForm.Show();
                btnMenuSelected.Text = btnMenu1.Text + btnMenu1_sub3.Text;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Form1>().Count() == 0)
            {
                Form1 objForm = new Form1();
                objForm.TopLevel = false;
                pnlContent.Controls.Add(objForm);
                //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //objForm.Dock = DockStyle.Fill;
                objForm.Show();
                btnMenuSelected.Text = btnMenu2.Text + button6.Text;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Form1>().Count() == 0)
            {
                Form1 objForm = new Form1();
                objForm.TopLevel = false;
                pnlContent.Controls.Add(objForm);
                //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //objForm.Dock = DockStyle.Fill;
                objForm.Show();
                btnMenuSelected.Text = btnMenu2.Text + button5.Text;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Form1>().Count() == 0)
            {
                Form1 objForm = new Form1();
                objForm.TopLevel = false;
                pnlContent.Controls.Add(objForm);
                //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //objForm.Dock = DockStyle.Fill;
                objForm.Show();
                btnMenuSelected.Text = btnMenu2.Text + button3.Text;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Form1>().Count() == 0)
            {
                Form1 objForm = new Form1();
                objForm.TopLevel = false;
                pnlContent.Controls.Add(objForm);
                //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //objForm.Dock = DockStyle.Fill;
                objForm.Show();
                btnMenuSelected.Text = btnMenu3.Text + btnMenu3_sub1.Text;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Form1>().Count() == 0)
            {
                Form1 objForm = new Form1();
                objForm.TopLevel = false;
                pnlContent.Controls.Add(objForm);
                //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //objForm.Dock = DockStyle.Fill;
                objForm.Show();
                btnMenuSelected.Text = btnMenu3.Text + btnMenu3_sub2.Text;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<Form1>().Count() == 0)
            {
                Form1 objForm = new Form1();
                objForm.TopLevel = false;
                pnlContent.Controls.Add(objForm);
                //objForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                //objForm.Dock = DockStyle.Fill;
                objForm.Show();
                btnMenuSelected.Text = btnMenu3.Text + btnMenu3_sub3.Text;
            }
        }

        private void btnMenu3_Click(object sender, EventArgs e)
        {
            showsubMenu(pnlMenu3_sub);
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            customizeDesign();
            btnMenuSelected.Text = btnHome.Text;
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            showsubMenu(pnlMenu);
            //btnMenu.IconColor ="#333333";
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            showsubMenu(pnlMenu1_sub);
        }

        private void btnMenu1_Click(object sender, EventArgs e)
        {

        }
    }
}
