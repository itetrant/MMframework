﻿using MMframework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MMFramework
{
    public partial class frmLogin : Form
    {
        public bool IsLoggedIn { get; set; }
        public String iUserName { get; set; }
        public frmLogin()
        {
            InitializeComponent();
        }

        private void Form_load(object sender, EventArgs e)
        {

        }
 
        private void btnLogin_Click(object sender, EventArgs e)
        {
            IsLoggedIn = true;
            iUserName = txtUser.Text;
            this.Close();
        }

        private void txtUser_TextChanged(object sender, EventArgs e)
        {
            if (txtUser.Text == "Username")
            {
                txtUser.Text ="";
            }
        }

        private void txtUser_Enter(object sender, EventArgs e)
        {
            if (txtUser.Text == "Username")
            {
                txtUser.Text = "";
            }
        }

        private void txtUser_Leave(object sender, EventArgs e)
        {
            if (txtUser.Text == "")
            {
                txtUser.Text = "Username";
            }
        }

        private void txtPass_Enter(object sender, EventArgs e)
        {
            if (txtPass.Text == "Password")
            {
                txtPass.Text = "";
                txtPass.UseSystemPasswordChar = true;
            }
        }

        private void txtPass_Leave(object sender, EventArgs e)
        {
            if (txtPass.Text == "")
            {
                txtPass.Text = "Password";
                txtPass.UseSystemPasswordChar = false;
            }
        }

        private void frmLogin_Load_1(object sender, EventArgs e)
        {

        }

        private void btnEN_Click(object sender, EventArgs e)
        {
            btnEN.Visible = false;
            btnVN.Visible = true;
            chkRemmember.Text = "Ghi nhớ đăng nhập của tôi";
            btnLogin.Text = "ĐĂNG NHẬP";
        }

        private void btnVN_Click(object sender, EventArgs e)
        {
            btnVN.Visible = false;
            btnEN.Visible = true;
            chkRemmember.Text = "Remember my login";
            btnLogin.Text = "LOG IN";
        }

        private void txtPass_KeyPress(object sender, KeyPressEventArgs e)
        {
           //
        }
    }
}
